﻿using Customer.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Customer.Domain.AggregatesModel.CustomerAggregae
{
    public class CustomerItem : Entity, IAggregateRoot
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
