﻿using Customer.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Domain.AggregatesModel.CustomerAggregae
{
    public interface ICustomerItemRepository : IRepository<CustomerItem>
    {
        CustomerItem Add(CustomerItem buyer);
        CustomerItem Update(CustomerItem buyer);
        //Task<CustomerItem> FindAsync(string BuyerIdentityGuid);
        Task<CustomerItem> FindByIdAsync(string id);
    }
}
