﻿using Customer.Domain.AggregatesModel.BuyerAggregate;
using Customer.Domain.AggregatesModel.CustomerAggregae;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Customer.Infrastructure.EntityConfigurations
{
    class CustomerItemEntityTypeConfiguration
        : IEntityTypeConfiguration<CustomerItem>
    {
        public void Configure(EntityTypeBuilder<CustomerItem> customerItemConfiguration)
        {
            customerItemConfiguration.ToTable("customerItem", CustomerContext.DEFAULT_SCHEMA);

            customerItemConfiguration.HasKey(b => b.Id);

            customerItemConfiguration.Ignore(b => b.DomainEvents);

            customerItemConfiguration.Property(b => b.Id)
                .ForSqlServerUseSequenceHiLo("customeritemseq", CustomerContext.DEFAULT_SCHEMA);

            customerItemConfiguration.Property(b => b.Age);
            
            customerItemConfiguration.Property(b => b.Name)
                .HasMaxLength(256)
                .IsRequired();

        }
    }
}
