﻿using Customer.Domain.AggregatesModel.CustomerAggregae;
using Customer.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Customer.Infrastructure.Repositories
{
    public class CustomerItemRepository
        : ICustomerItemRepository
    {
        private readonly CustomerContext _context;
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public CustomerItemRepository(CustomerContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public CustomerItem Add(CustomerItem customerItem)
        {
            if (customerItem.IsTransient())
            {
                return _context.CustomerItems
                    .Add(customerItem)
                    .Entity;
            }
            else
            {
                return customerItem;
            }
        }

        public CustomerItem Update(CustomerItem customerItem)
        {
            return _context.CustomerItems
                    .Update(customerItem)
                    .Entity;
        }

        //public async Task<CustomerItem> FindAsync(string identity)
        //{
        //    var buyer = await _context.Buyers
        //        .Include(b => b.PaymentMethods)
        //        .Where(b => b.IdentityGuid == identity)
        //        .SingleOrDefaultAsync();

        //    return buyer;
        //}

        public async Task<CustomerItem> FindByIdAsync(string id)
        {
            var customerItem = await _context.CustomerItems
                .Where(b => b.Id == int.Parse(id))
                .SingleOrDefaultAsync();

            return customerItem;
        }
    }
}
