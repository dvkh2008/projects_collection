﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWithOpenXML.Helpers.Excel
{
    public class ExcelHelperSheetInfo
    {
        public ExcelHelperSheetInfo(string name)
        {
            Name = name;
            Configs = new List<IExcelHelperConfig>();
        }

        public ExcelHelperSheetInfo(IEnumerable<IExcelHelperConfig> configs)
        {
            Configs = configs;
        }

        public ExcelHelperSheetInfo(string name, IEnumerable<IExcelHelperConfig> configs)
        {
            Name = name;
            Configs = configs;
        }

        public string Name { get; set; }
        public IEnumerable<IExcelHelperConfig> Configs { get; set; }
    }
}
