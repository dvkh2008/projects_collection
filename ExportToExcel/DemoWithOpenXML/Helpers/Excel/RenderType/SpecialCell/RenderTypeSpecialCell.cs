﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Spreadsheet;

namespace DemoWithOpenXML.Helpers.Excel.RenderType.SpecialCell
{
    public class RenderTypeSpecialCell : IRenderType
    {
        public Task Render(SheetData sheetData, IExcelHelperConfig config)
        {
            Render2(sheetData, (ExcelHelperConfigRenderSpecialCell)config);
            return Task.CompletedTask;
        }

        public void Render2(SheetData sheetData, ExcelHelperConfigRenderSpecialCell config)
        {
            var cellName = ExcelHelperExtension.GetExcelCellName(config.CellIndex, config.RowIndex);
            var cellInfo = sheetData.Descendants<Cell>().Where(c => c.CellReference == cellName).FirstOrDefault();

            if (cellInfo == null) return;

            cellInfo.CellValue = new CellValue(config.Value);
            cellInfo.
        }

    }
}
