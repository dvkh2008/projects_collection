﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWithOpenXML.Helpers.Excel.RenderType.SpecialCell
{
    public class ExcelHelperConfigRenderSpecialCell: IExcelHelperConfig
    {
        public int RowIndex { get; set; }
        public int CellIndex { get; set; }
        public string Format { get; set; }
        public string Value { get; set; }
    }
}
