﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWithOpenXML.Helpers.Excel.RenderType.ObjectList
{
    public class ExcelHelperConfigObjectList: IExcelHelperConfig
    {
        public ExcelHelperConfigObjectList(List<string> propertiesName, IEnumerable<object> data)
        {
            PropertiesName = propertiesName;
            Data = data;
        }

        public int BeginRowIndex { get; set; }
        public int BeginCellIndex { get; set; }

        public List<string> PropertiesName { get; set; }
        public IEnumerable<object> Data { get; set; }
    }
}
