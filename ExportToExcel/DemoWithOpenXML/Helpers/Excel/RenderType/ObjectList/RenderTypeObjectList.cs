﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace DemoWithOpenXML.Helpers.Excel.RenderType.ObjectList
{
    public class RenderTypeObjectList : IRenderType
    {
        public Task Render(SheetData sheetData, IExcelHelperConfig config)
        {
            Render2(sheetData, (ExcelHelperConfigObjectList)config);
            return Task.CompletedTask;
        }

        private void Render2(SheetData sheetData, ExcelHelperConfigObjectList config)
        {
            var _beginCellIndex = config.BeginCellIndex;
            var _beginRowIndex = config.BeginRowIndex;
            var stylesIndex = GetStylesDataTableTemplate(sheetData, config);
            var propCount = config.PropertiesName.Count;

            foreach (var dataRow in config.Data)
            {
                var rowData = new Row()
                {
                    RowIndex = Convert.ToUInt32(_beginRowIndex)
                };

                for (int propIndex = 0; propIndex < propCount; propIndex++)
                {
                    var propertyName = config.PropertiesName[propIndex];
                    var cellName = ExcelHelperExtension.GetExcelCellName(_beginCellIndex, _beginRowIndex);
                    var cell = SetCell(cellName, GetPropertyValue(dataRow, propertyName), stylesIndex.ElementAt(propIndex));
                    rowData.Append(cell);

                    _beginCellIndex++;
                }
                _beginCellIndex = config.BeginCellIndex;
                _beginRowIndex++;
                sheetData.Append(rowData);
            }
        }

        private Cell SetCell(string columnName, string cellVale, UInt32Value styleIndex)
        {
            return new Cell()
            {
                CellReference = columnName,
                CellValue = new CellValue(cellVale),
                DataType = CellValues.SharedString,
                StyleIndex = styleIndex
            };
        }

        private static IEnumerable<UInt32Value> GetStylesDataTableTemplate(SheetData sheetData, ExcelHelperConfigObjectList config)
        {
            var rowStart = sheetData.Elements<Row>().Where(r => r.RowIndex == config.BeginRowIndex).FirstOrDefault();
            IList<UInt32Value> stylesIndex = new List<UInt32Value>();

            var length = config.PropertiesName.Count();
            for (var i = 0; i < length; i++)
            {
                Cell cell = rowStart.Elements<Cell>().ElementAt(i);
                stylesIndex.Add(cell.StyleIndex);
            }
            return stylesIndex.AsEnumerable();
        }

        private string GetPropertyValue(object src, string propertyName)
        {
            return src.GetType().GetProperty(propertyName).GetValue(src, null)?.ToString();
        }
    }
}
