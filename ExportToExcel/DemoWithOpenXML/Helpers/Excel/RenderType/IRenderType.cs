﻿using DocumentFormat.OpenXml.Spreadsheet;
using System.Threading.Tasks;

namespace DemoWithOpenXML.Helpers.Excel.RenderType
{
    public interface IRenderType
    {
        Task Render(SheetData sheetData, IExcelHelperConfig config);
    }
}
