﻿using DemoWithOpenXML.Helpers.Excel.RenderType.ObjectList;
using DemoWithOpenXML.Helpers.Excel.RenderType.SpecialCell;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWithOpenXML.Helpers.Excel
{
    public class ExcelHelper : IDisposable
    {
        private string _templateFilePath;
        private MemoryStream stream;

        public ExcelHelper(string templateFilePath)
        {
            _templateFilePath = templateFilePath;
        }

        public async Task<MemoryStream> Render(IEnumerable<ExcelHelperSheetInfo> sheetInfos)
        {
            byte[] fileBytes = await File.ReadAllBytesAsync(_templateFilePath);
            stream = new MemoryStream();
            await stream.WriteAsync(fileBytes, 0, fileBytes.Length);

            var sheetIndex = 0;

            using (var spreadsheet = SpreadsheetDocument.Open(stream, true))
            {
                WorkbookPart wbPart = spreadsheet.WorkbookPart;
                foreach (var sheetInfo in sheetInfos)
                {
                    var theSheet = wbPart.Workbook.Descendants<Sheet>().ElementAt(sheetIndex);
                    if(theSheet == null)
                    {
                        sheetIndex++;
                        continue;
                    }

                    WorksheetPart wsPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));

                    var cellinfo = wsPart.Worksheet.Descendants<Cell>().Where(c => c.CellReference == "A1").FirstOrDefault();
                    var cellinfo2 = wsPart.Worksheet.Descendants<Cell>().Where(c => c.CellReference == "A2").FirstOrDefault();

                     var aaa = ExcelHelperExtension.GetCellValue(_templateFilePath, "Search Results", "A2");


                    var sheetData = wsPart.Worksheet.Elements<SheetData>().First();

                    foreach (var config in sheetInfo.Configs)
                    {
                        var configType = config.GetType();

                        if (configType == typeof(ExcelHelperConfigObjectList))
                        {
                            await new RenderTypeObjectList().Render(sheetData, config);
                        }
                        else if (configType == typeof(ExcelHelperConfigRenderSpecialCell))
                        {
                            await new RenderTypeSpecialCell().Render(sheetData, config);
                        }
                    }

                    sheetIndex++;
                }

                spreadsheet.Close();
            }

            return stream;
        }

        public void Dispose()
        {
            _templateFilePath = null;
            if (stream != null)
                stream.Dispose();
        }
    }
}
