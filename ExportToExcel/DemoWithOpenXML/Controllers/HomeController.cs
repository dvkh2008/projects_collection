﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DemoWithOpenXML.Helpers.Excel;
using DemoWithOpenXML.Helpers.Excel.RenderType.SpecialCell;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace DemoWithOpenXML.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public HomeController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> Index()
        {

            var templateFilePath = Path.Combine(_hostingEnvironment.ContentRootPath, "download-supplier-results.xlsx");
            using (var excelHelper = new ExcelHelper(templateFilePath))
            {

                var sheetInfos = new List<ExcelHelperSheetInfo>();

                #region [Search Result Sheet]
                var sheetSearchResult = new ExcelHelperSheetInfo("Search Results");
                var configSearchResult = new List<IExcelHelperConfig>();
                var configSearchBy = new ExcelHelperConfigRenderSpecialCell()
                {
                    CellIndex = 1,
                    RowIndex = 2,
                    Value = "Dvkh2008"
                };
                configSearchResult.Add(configSearchBy);

                sheetSearchResult.Configs = configSearchResult;
                sheetInfos.Add(sheetSearchResult);
                #endregion

                var result = await excelHelper.Render(sheetInfos);
                return File(result.ToArray(), "application/pdf", $"Test_Result.xlsx");

            }
        }
    }
}