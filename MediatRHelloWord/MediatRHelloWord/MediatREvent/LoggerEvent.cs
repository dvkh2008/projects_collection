﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediatRHelloWord.MediatREvent
{
    public class LoggerEvent: INotification
    {
        public string Message { get; set; }

        public LoggerEvent(string message)
        {
            this.Message = message;
        }
    }
}
