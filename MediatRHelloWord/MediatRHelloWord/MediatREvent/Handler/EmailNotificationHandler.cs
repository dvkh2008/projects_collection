﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MediatRHelloWord.MediatREvent.Handler
{
    public class EmailNotificationHandler : INotificationHandler<LoggerEvent>
    {
        public Task Handle(LoggerEvent notification, CancellationToken cancellationToken)

        {
            string message = notification.Message;

            LogtoEmail(message);

            return Task.FromResult(0);

        }

        private void LogtoEmail(string message)
        {
            Console.WriteLine($"EmailNotificationHandler: {message}");
        }
    }
}
