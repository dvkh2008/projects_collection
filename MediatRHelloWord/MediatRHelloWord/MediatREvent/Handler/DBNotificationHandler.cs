﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MediatRHelloWord.MediatREvent.Handler
{
    public class DBNotificationHandler : INotificationHandler<LoggerEvent>
    {
        public Task Handle(LoggerEvent notification, CancellationToken cancellationToken)

        {
            string message = notification.Message;

            LogtoDB(message);

            return Task.FromResult(0);

        }

        private void LogtoDB(string message)
        {
            Console.WriteLine($"DBNotificationHandler: {message}");
        }
    }
}
