﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MediatRHelloWord.MediatREvent.Handler
{
    public class FileNotificationHandler : INotificationHandler<LoggerEvent>
    {
        public Task Handle(LoggerEvent notification, CancellationToken cancellationToken)

        {
            string message = notification.Message;

            LogtoFile(message);

            return Task.FromResult(0);

        }

        private void LogtoFile(string message)
        {
            Console.WriteLine($"FileNotificationHandler: {message}");
        }
    }
}
