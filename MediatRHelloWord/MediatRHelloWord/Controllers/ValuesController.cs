﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MediatRHelloWord.MediatREvent;
using Microsoft.AspNetCore.Mvc;

namespace MediatRHelloWord.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ValuesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("handlers")]
        public async Task<ActionResult> Handlers(CancellationToken cancellationToken)
        {
            await _mediator.Publish(new LoggerEvent($"check logger multiple handlers {DateTime.Now.ToLongTimeString()}"), cancellationToken);
            return Ok("Ok");
        }
    }
}
