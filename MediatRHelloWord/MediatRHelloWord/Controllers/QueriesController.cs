﻿using MediatR;
using MediatRHelloWord.Queries.ProductByIdQueryAsync;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediatRHelloWord.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueriesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public QueriesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("productbyidqueryasync")]
        public async Task<IActionResult> ProductByIdQueryAsync()
        {
            var query = new ProductByIdQueryAsync { Id = 1 };
            var plastico = await _mediator.Send(query);

            return Ok(plastico);
        }

    }
}
