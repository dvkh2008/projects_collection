﻿using System;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace MediatRHelloWord.Notification
{
    public class ProductSavedNotificationAsyncFirstHandler : INotificationHandler<ProductSavedNotificationAsync>
    {
        public Task Handle(ProductSavedNotificationAsync notification, CancellationToken cancellationToken)
        {
            return Task.Run(() => Console.WriteLine("ProductSavedNotificationAsyncFirstHandler.Handle(ProductSavedNotificationAsync)"));
        }
    }
}
