﻿using MediatR;

namespace MediatRHelloWord.Notification
{
    public class ProductSavedNotificationAsync : INotification
    {
        public long Id { get; set; }
    }
}
