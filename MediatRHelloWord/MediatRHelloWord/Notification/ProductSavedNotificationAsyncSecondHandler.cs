﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MediatRHelloWord.Notification
{
    public class ProductSavedNotificationAsyncSecondHandler : INotificationHandler<ProductSavedNotificationAsync>
    {
        public Task Handle(ProductSavedNotificationAsync notification, CancellationToken cancellationToken)
        {
            return Task.Run(() => Console.WriteLine("ProductSavedNotificationAsyncSecondHandler.Handle(ProductSavedNotificationAsync)"));
        }
    }
}
