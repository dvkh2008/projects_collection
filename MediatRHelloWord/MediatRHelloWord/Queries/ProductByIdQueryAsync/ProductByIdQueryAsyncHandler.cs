﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MediatRHelloWord.Queries.ProductByIdQueryAsync
{
    public class ProductByIdQueryAsyncHandler : IRequestHandler<ProductByIdQueryAsync, ProductByIdQueryAsyncResult>
    {
        public Task<ProductByIdQueryAsyncResult> Handle(ProductByIdQueryAsync request, CancellationToken cancellationToken)
        {
            Console.WriteLine("ProductByIdQueryAsyncHandler.Handle(ProductByIdQueryAsync)");
            return Task.FromResult(new ProductByIdQueryAsyncResult { Id = request.Id, Description = $"Description {request.Id}" });
        }
    }
}
