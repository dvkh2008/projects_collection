﻿using MediatR;

namespace MediatRHelloWord.Commands.Request
{
    public class ProductSaveCommand : IRequest
    {
        public long Id { get; set; }

        public string Description { get; set; }
    }
}
