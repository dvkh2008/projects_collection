﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MediatRHelloWord.Commands.Request
{
    public class ProductSaveCommandAsyncHandler : AsyncRequestHandler<ProductSaveCommandAsync>
    {
        protected override Task Handle(ProductSaveCommandAsync request, CancellationToken cancellationToken)
        {
            return Task.Run(() => Console.WriteLine("ProductSaveCommandHandler.Handle(ProductSaveCommand)"));
        }
    }
}
