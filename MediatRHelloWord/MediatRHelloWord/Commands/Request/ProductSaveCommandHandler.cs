﻿using MediatR;
using System;

namespace MediatRHelloWord.Commands.Request
{
    public class ProductSaveCommandHandler : RequestHandler<ProductSaveCommand>
    {
        protected override void Handle(ProductSaveCommand request)
        {
            Console.WriteLine("ProductSaveCommandHandler.Handle(ProductSaveCommand)");
        }
    }
}
