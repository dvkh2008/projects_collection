﻿using iText.Layout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWithIText7.Helpers.Pdf.RenderWith
{
    public interface IRenderWith
    {
        Task Render(Document doc, IPdfHelperConfig config);
    }
}
