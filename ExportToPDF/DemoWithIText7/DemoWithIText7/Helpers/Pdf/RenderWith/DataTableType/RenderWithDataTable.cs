﻿using System.Threading.Tasks;
using iText.Layout;
using iText.Layout.Element;

namespace DemoWithIText7.Helpers.Pdf.RenderWith.DataTableType
{
    public class RenderWithDataTable : IRenderWith
    {
        public Task Render(Document doc, IPdfHelperConfig config)
        {
            doc.Add(CreateTable((PdfHelperConfigDataTable)config));
            return Task.CompletedTask;
        }

        private Table CreateTable(PdfHelperConfigDataTable config)
        {
            if (config.Data == null) return new Table(1);

            var columnCount = config.Data.Columns.Count;
            var rowCount = config.Data.Rows.Count;

            var table = new Table(columnCount);
            config.ConfigTable?.Invoke(table);

            if (config.IsShowHeader)
            {
                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    var columnName = config.Data.Columns[columnIndex].ColumnName;
                    var cell = config.CreateCellHeader?.Invoke(columnIndex, columnName) ?? (new Cell()).Add(new Paragraph(columnName));
                    table.AddHeaderCell(cell);
                }
            }

            for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
            {
                var row = config.Data.Rows[rowIndex];
                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    var columnData = row[columnIndex];
                    if (columnData != null)
                    {
                        var columnName = columnData.ToString();
                        var cell = config.CreateCellBody?.Invoke(rowIndex, columnIndex, columnName) ?? (new Cell()).Add(new Paragraph(columnName));
                        table.AddCell(cell);
                    }
                }
            }

            return table;
        }
    }
}
