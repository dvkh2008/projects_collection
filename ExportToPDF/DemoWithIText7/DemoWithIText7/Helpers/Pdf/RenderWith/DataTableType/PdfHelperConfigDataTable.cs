﻿using iText.Layout.Element;
using System;
using System.Data;

namespace DemoWithIText7.Helpers.Pdf.RenderWith.DataTableType
{
    public class PdfHelperConfigDataTable : IPdfHelperConfig
    {
        public DataTable Data { get; set; }
        public bool IsShowHeader { get; set; }
        public Action<Table> ConfigTable { get; set; }
        public Func<int, string, Cell> CreateCellHeader { get; set; }
        public Func<int, int, string, Cell> CreateCellBody { get; set; }
    }
}
