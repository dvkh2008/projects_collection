﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iText.Layout;
using iText.Layout.Element;

namespace DemoWithIText7.Helpers.Pdf.RenderWith.NewPage
{
    public class RenderWithNewPage : IRenderWith
    {
        public Task Render(Document doc, IPdfHelperConfig config)
        {
            var areaBread = new AreaBreak();
            doc.Add(areaBread);
            return Task.CompletedTask;
        }
    }
}
