﻿using iText.IO.Font.Constants;
using iText.Kernel.Events;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using System;
using System.Collections.Generic;
using System.IO;

namespace DemoWithIText7.Helpers.Pdf
{
    public class PdfEventShowPageNumber : IEventHandler
    {
        protected Document _doc;
        protected int _pageCount;

        public PdfEventShowPageNumber(Document doc, int pageCount)
        {
            _doc = doc;
            _pageCount = pageCount;
        }

        public void HandleEvent(Event @event)
        {
            PdfDocumentEvent documentEvent = (PdfDocumentEvent)@event;
            var pdf = documentEvent.GetDocument();
            PdfPage page = documentEvent.GetPage();
            Rectangle pageSize = page.GetPageSize();

            var pageNumber = $"Page {pdf.GetPageNumber(page)} of {_pageCount}";

            new PdfCanvas(page)
                    .BeginText()
                    .SetFontAndSize(PdfFontFactory.CreateFont(StandardFonts.HELVETICA), 12)
                    .MoveText((pageSize.GetRight() - _doc.GetRightMargin() - (pageSize.GetLeft() + _doc.GetLeftMargin())) / 2 + _doc.GetLeftMargin(), pageSize.GetTop() - _doc.GetTopMargin() + 10)
                    .ShowText(pageNumber)
                    .MoveText(0, (pageSize.GetBottom() + _doc.GetBottomMargin()) - (pageSize.GetTop() + _doc.GetTopMargin()) - 20)
                    .ShowText(pageNumber)
                    .EndText()
                    .Release();
                    //.Stroke();
            ///

            //PdfCanvas canvas = new PdfCanvas(page);
            
            //canvas.BeginText();
            //try
            //{
            //    canvas.SetFontAndSize(PdfFontFactory.CreateFont(StandardFonts.HELVETICA), 5);
            //}
            //catch (IOException e)
            //{
                
            //}
            //canvas.MoveText((pageSize.GetRight() - _doc.GetRightMargin() - (pageSize.GetLeft() + _doc.GetLeftMargin())) / 2 + _doc.GetLeftMargin(), pageSize.GetTop() - _doc.GetTopMargin() + 10))
            //        .showText("this is a header")
            //        .moveText(0, (pageSize.GetBottom() + _doc.GetBottomMargin()) - (pageSize.GetTop() + _doc.GetTopMargin()) - 20)
            //        .showText("this is a footer")
            //        .endText()
            //        .release();
        }
    }
}
