﻿using DemoWithIText7.Helpers.Pdf.RenderWith.DataTableType;
using DemoWithIText7.Helpers.Pdf.RenderWith.NewPage;
using iText.Kernel.Events;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DemoWithIText7.Helpers.Pdf
{
    public static class PdfHelper
    {
        public static void Save(Stream os, IEnumerable<IPdfHelperConfig> configs)
        {
            var pageCount = 0;
            using (var _os = new MemoryStream())
            {
                pageCount = Save2(_os, configs, pageCount);
            }

            Save2(os, configs, pageCount);
        }

        private static int Save2(Stream os, IEnumerable<IPdfHelperConfig> configs, int pageCount)
        {
            PdfWriter writer = new PdfWriter(os);
            var pdf = new PdfDocument(writer);
            var document = new Document(pdf);
            pdf.SetDefaultPageSize(new PageSize(new Rectangle(841.8f, 595.2f)));

            var handler = new PdfEventShowPageNumber(document, pageCount);
            pdf.AddEventHandler(PdfDocumentEvent.START_PAGE, handler);

            foreach (var config in configs)
            {
                var configType = config.GetType();
                if (configType == typeof(PdfHelperConfigDataTable))
                {
                    new RenderWithDataTable().Render(document, config);
                }
                else if (configType == typeof(PdfHelperConfigNewPage))
                {
                    new RenderWithNewPage().Render(document, config);
                }
            }
            pageCount = pdf.GetNumberOfPages();
            document.Close();
            writer.Close();

            return pageCount;
        }

    }
}
