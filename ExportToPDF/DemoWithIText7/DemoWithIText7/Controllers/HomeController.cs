﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using DemoWithIText7.Models;
using System.IO;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Kernel.Colors;
using System.Data;
using DemoWithIText7.Helpers.Pdf;
using System.Collections.Generic;
using DemoWithIText7.Helpers.Pdf.RenderWith.DataTableType;
using DemoWithIText7.Helpers.Pdf.RenderWith.NewPage;

namespace DemoWithIText7.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        private DataTable CreateDataTable(string context)
        {
            int columnCount = 10, rowCount = 30;
            var dt = new DataTable();

            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
            {
                dt.Columns.Add(new DataColumn($"Column {columnIndex} {context}", typeof(string)));
            }

            for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
            {
                var row = dt.NewRow();
                for (int columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    row[columnIndex] = $"Row {rowIndex} - {columnIndex} - {context}";
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        public IActionResult DownloadPDF()
        {
            var pdfHelperConfig = new List<IPdfHelperConfig>
            {
                new PdfHelperConfigDataTable()
                {
                    Data = CreateDataTable("A"),
                    ConfigTable = (Table table) => {
                    },
                    IsShowHeader = true,
                    CreateCellHeader = (int columnIndex, string columnName) =>
                    {
                        var p = new Paragraph(columnName);
                        return new Cell().Add(p)
                        .SetFontColor(ColorConstants.WHITE)
                        .SetBackgroundColor(ColorConstants.BLACK)
                        .SetPaddingLeft(5);
                    },
                    CreateCellBody = (int rowIndex, int columnIndex, string columnName) =>
                    {
                        var p = new Paragraph(columnName);
                        return new Cell().Add(p)
                        //.SetFontColor(ColorConstants.WHITE)
                        //.SetBackgroundColor(ColorConstants.BLACK)
                        .SetPaddingLeft(5);
                    }
                },
                new PdfHelperConfigNewPage(),
                new PdfHelperConfigDataTable()
                {
                    Data = CreateDataTable("B"),
                    ConfigTable = (Table table) => {
                    },
                    IsShowHeader = true,
                    CreateCellHeader = (int columnIndex, string columnName) =>
                    {
                        var p = new Paragraph(columnName);
                        return new Cell().Add(p)
                        .SetFontColor(ColorConstants.WHITE)
                        .SetBackgroundColor(ColorConstants.BLACK)
                        .SetPaddingLeft(5);
                    },
                    CreateCellBody = (int rowIndex, int columnIndex, string columnName) =>
                    {
                        var p = new Paragraph(columnName);
                        return new Cell().Add(p)
                        //.SetFontColor(ColorConstants.WHITE)
                        //.SetBackgroundColor(ColorConstants.BLACK)
                        .SetPaddingLeft(5);
                    }
                }
            };

            using (var stream = new MemoryStream())
            {
                PdfHelper.Save(stream, pdfHelperConfig);
                return File(stream.ToArray(), "application/pdf", "DownloadPDF.pdf");
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
